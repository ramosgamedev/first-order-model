import imageio
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import sys
import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'

from skimage.transform import resize
from IPython.display import HTML
import warnings
warnings.filterwarnings("ignore")

from demo import load_checkpoints
generator, kp_detector = load_checkpoints(config_path='config/vox-256.yaml', 
                            checkpoint_path='settings/vox-cpk.pth.tar')



from demo import make_animation
from skimage import img_as_ubyte

entrada_ = "content/entrada.png"
salida_ = "content/salida.mp4"
salidaVideo = 'content/result_output.mp4'

if (len(sys.argv) > 1):
    entrada_ = "content/" + str(sys.argv[1])
if (int(len(sys.argv)) > 2):
    salida_ = "content/" + str(sys.argv[2])
if (int(len(sys.argv)) > 3):
    salidaVideo = "content/" + str(sys.argv[3])

print(entrada_)
print(salida_)
print(salidaVideo)

source_image = imageio.imread(entrada_)
reader = imageio.get_reader(salida_)
 
fps = reader.get_meta_data()['fps']
driving_video = []
try:
    for im in reader:
        driving_video.append(im)
except RuntimeError:
    pass
reader.close()
 
#Resize image and video to 256x256
 
source_image = resize(source_image, (256, 256))[..., :3]
driving_video = [resize(frame, (256, 256))[..., :3] for frame in driving_video]
 
predictions = make_animation(source_image, driving_video, generator, kp_detector, relative=True,
                             adapt_movement_scale=True)
#save resulting video
imageio.mimsave(salidaVideo, [img_as_ubyte(frame) for frame in predictions])
